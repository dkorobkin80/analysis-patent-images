from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import datetime
import numpy as np
import time
import re

yandex_url_pattern = 'https://yandex.ru/patents?dco=RU&dco=SU&dfr={date}&dl=ru&dt=0&dto={date}&dty=2&s=1&sp={page}&spp=500&st=0'
chromedriver_file_name = '/Users/viktor/Documents/chromedriver/chromedriver.exe'
patents_dir = r'patents/'
docs_on_page = 500

driver = webdriver.Chrome(ChromeDriverManager().install())
#driver = webdriver.Chrome(chromedriver_file_name)


def save_links(links, year):
    with open(patents_dir + str(year) + '.txt', 'a') as f:
        for link in links:
            f.write(link + '\n')


def get_links(driver, date, page):
    driver.get(yandex_url_pattern.format(date=date, page=page))
    time.sleep(10)
    return re.findall(r'"/patents/doc/[^"]*"', driver.page_source)


current_date = datetime.datetime.strptime('2022.01.01', '%Y.%m.%d')
end_time = datetime.datetime.strptime('2022.12.31', '%Y.%m.%d')

while current_date <= end_time:
    date = current_date.strftime('%Y.%m.%d')
    links = get_links(driver, date=date, page=0)
    documents_search = re.search(r'Нашлось документов: [0-9]+', driver.page_source)

    if documents_search is None:
        documents_number = 0
    else:
        number_part = documents_search.group(0).split(':')[1]
        documents_number = int(number_part)

    pages_number = (documents_number - 1) // docs_on_page + 1
    left_pages = pages_number - 1
    print('Date #:', date, '\t', 'Links #:', len(links), '\t', 'Documents #:',
          documents_number, '\t', 'Left pages:', left_pages)
    save_links(links, current_date.year)

    for page in range(1, left_pages + 1):
        links = get_links(driver, date=date, page=page)
        print('---', 'Links #:', len(links), 'Page :', page)
        save_links(links, current_date.year)

    current_date += datetime.timedelta(days=1)
