from patent_fields import PatentFields
from typing import *


class Patent:
    def __init__(self, patent_link: str, patent_parser: PatentFields) -> None:
        self.__patent_link = patent_link
        self.__patent_parser = patent_parser
        self.patent_link = self.__patent_link

    @property
    def patent_link(self) -> str:
        return self.__patent_link

    @patent_link.setter
    def patent_link(self, patent_link) -> None:
        self.__patent_link = patent_link
        self.__patent_parser.set_patent_link(patent_link)

    @property
    def patent_parser(self) -> PatentFields:
        return self.__patent_parser

    @patent_parser.setter
    def patent_parser(self, patent_parser: PatentFields) -> None:
        self.__patent_parser = patent_parser
        self.__patent_parser.set_patent_link(self.__patent_link)

    def get_id(self) -> str:
        """
        Получить ID
        :return: ID в текстовом виде
        """
        return self.__patent_parser.get_id()

    def get_title(self) -> str:
        """
        Получить заголовок
        :return: Заголовок
        """
        return self.__patent_parser.get_title()

    def get_assignee(self) -> str:
        """
        Получить правопреемника
        :return: Правопреемник
        """
        return self.__patent_parser.get_assignee()

    def get_authors(self) -> List[str]:
        """
        Получить автора
        :return: Список авторов
        """
        return self.__patent_parser.get_authors()

    def get_publications(self) -> List[str]:
        """
        Получить список дат публикаций
        :return: Список дат в виде строки
        """
        return self.__patent_parser.get_publications()

    def get_abstract(self) -> str:
        """
        Получить аннотацию
        :return: Текст аннотации
        """
        return self.__patent_parser.get_abstract()

    def get_description(self) -> str:
        """
        Получить описание
        :return: Текст описания
        """
        return self.__patent_parser.get_description()

    def get_claims(self) -> str:
        """
        Получить формулы изобретения
        :return: Текст формул изобретения
        """
        return self.__patent_parser.get_claims()

    def get_classifications(self) -> List[str]:
        """
        Получить классы по классификации CPC
        :return: Список классов
        """
        return self.__patent_parser.get_classification()

    def get_cited(self) -> List[str]:
        """
        Получить список ID патентов, цитирующих данный патент
        :return: Список идентификаторов патентов
        """
        return self.__patent_parser.get_cited()
    




