import os
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from webdriver_manager.chrome import ChromeDriverManager
from os import walk

patents_directory = 'patents/'


def parse_images(patent_folder):
    """
    Парсит и сохраняет на локальной машине патентные изображения в папку с именем ID патента
    :param patent_folder: Папка с ID патента
    """
    
    url = f"https://yandex.ru{patent_folder}"
    
    html = browser.page_source
    soup = BeautifulSoup(html, "html.parser")
    # Проверка, присутствует ли на странице сообщение "По вашему запросу ничего не нашлось"
    no_results = soup.find("div", {"class": "error"})
    if no_results:
        print("No page found. Exiting program.")
        return
    # Получение ID патента из URL
    patent_id = url.split("/")[-1].split("_")[0]
    # Создание папки с ID патента для сохранения изображений
    folder_name = patent_id
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    # Загрузка страницы патента с помощью браузера Selenium
    browser.get(url)
    try:
        wait_again = WebDriverWait(browser, 15)
        wait_again.until(EC.presence_of_element_located((By.CLASS_NAME, "carousel-image")))
    except TimeoutException:
        print(f"Ошибка: на странице {url} не найдено изображение в илюстрации")
        # continue
    try:
        wait_asswel = WebDriverWait(browser, 15)
        wait_asswel.until(EC.presence_of_element_located((By.CLASS_NAME, "document-image")))
    except:
        print(f"Ошибка: на странице {url} не найдено изображение в содержании страницы")
    html = browser.page_source
    soup = BeautifulSoup(html, "html.parser")
    # Поиск всех тегов изображений на странице
    carousel_images = soup.find_all('img', {'class': 'carousel-image'})
    document_images = soup.find_all('img', {'class': 'document-image'})
    img_tags = carousel_images + document_images
    # Загрузка каждого изображения и сохранение его в папку с ID патента
    for i, img_tag in enumerate(img_tags):
        img_url = img_tag["src"]
        img_response = requests.get(img_url)
        img_filename = os.path.join(folder_name, f"{i+1}.jpg")
        with open(img_filename, "wb") as f:
            f.write(img_response.content)


def get_patent_links(file_name: str):
        with open(file_name, 'r') as f:
            return [line.strip().replace('"', '') for line in f]


browser = webdriver.Chrome(ChromeDriverManager().install())
_, _, year_files = next(walk(patents_directory))
years_list = list()

for year_file in year_files:
    years_list.append(get_patent_links(patents_directory +'/'+ year_file))
    
for year in years_list:
    for patent_links in year:
        parse_images(patent_links)

browser.quit()