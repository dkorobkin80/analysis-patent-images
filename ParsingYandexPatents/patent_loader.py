from webdriver import WebDriver
from bs4 import BeautifulSoup
from typing import *


yandex_patent_prefix = 'https://yandex.ru'


class PatentLoader:

    driver: WebDriver

    def __init__(self, web_driver: WebDriver) -> None:
        self.driver = web_driver

    def get_patent_links(self, file_name: str) -> List[str]:
        with open(file_name, 'r') as f:
            return [yandex_patent_prefix + line.strip().replace('"', '') for line in f]
