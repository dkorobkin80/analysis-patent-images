import os
import tensorflow as tf
from tensorflow.keras.preprocessing import image
import numpy as np

# For chem formulas
import pickle
import I2S_Model
import I2S_evalData
import deepsmiles

# For image conversion to Latex
from PIL import Image
from pix2tex.cli import LatexOCR


def predict_images(model_path, folder_path):
    """
    Определение класса патентного изображения
    :param model_path: Путь к сохраненной модели
    :param folder_path: Путь к папке с изображениями
    :return: Предсказанные метки
    """
    model = tf.saved_model.load(model_path)
    infer = model.signatures["serving_default"]
    
    class_labels = [0, 1, 2] # 0 - химическая, 1 - математическая формула, 2 - ни одна из формул
    
    for file_name in os.listdir(folder_path):
        if file_name.endswith('.jpg') or file_name.endswith('.jpeg') or file_name.endswith('.png'):
            img_path = os.path.join(folder_path, file_name)
            img = image.load_img(img_path, target_size=(100, 100))
            img_array = image.img_to_array(img)
            img_array = tf.expand_dims(img_array, 0)
            img_preprocessed = tf.keras.applications.resnet.preprocess_input(img_array)

            output = infer(conv2d_input=img_preprocessed)
            pred_class_idx = np.argmax(output['dense_2'][0])
            pred_class_label = class_labels[pred_class_idx]
            yield pred_class_label


#Evaluator
def evaluate(image_src, tokenizer, encoder, decoder):
    maxlength = 74 #should be determined from running the training script

    hidden = decoder.reset_state(batch_size=1)

    temp_input = tf.expand_dims(I2S_evalData.load_image(image_src)[0], 0)
    img_tensor_val = I2S_evalData.image_features_extract_model(temp_input)
    img_tensor_val = tf.reshape(img_tensor_val, (img_tensor_val.shape[0], -1, img_tensor_val.shape[3]))

    features = encoder(img_tensor_val)

    dec_input = tf.expand_dims([tokenizer.word_index['<start>']], 0)
    result = []

    for i in range(maxlength):
        predictions, hidden, attention_weights = decoder(dec_input, features, hidden)

        predicted_id = tf.argmax(predictions[0]).numpy()
        result.append(tokenizer.index_word[predicted_id])

        if tokenizer.index_word[predicted_id] == '<end>':
            return result

        dec_input = tf.expand_dims([predicted_id], 0)

    return result


def predictor(image_path):
    #Initialize DeepSMILES
    optimizer = tf.keras.optimizers.legacy.Adam(learning_rate=0.0005)
    #Prediction model parameters
    embedding_dim = 600
    units = 1024
    tokenizer = pickle.load(open("ParsingYandexPatents\\tokenizer.pkl","rb"))
    vocab_size = len(tokenizer.word_index) + 1
    encoder = I2S_Model.CNN_Encoder(embedding_dim)
    decoder = I2S_Model.RNN_Decoder(embedding_dim, units, vocab_size)

    checkpoint_path = "C:\\Fun\\ProstoParser\\Trained_Models"

    ckpt = tf.train.Checkpoint(encoder=encoder,decoder=decoder,optimizer = optimizer)
    ckpt_manager = tf.train.CheckpointManager(ckpt, checkpoint_path, max_to_keep=50)

    if ckpt_manager.latest_checkpoint:
        ckpt.restore(tf.train.latest_checkpoint(checkpoint_path))
        start_epoch = int(ckpt_manager.latest_checkpoint.split('-')[-1])

    result = evaluate(image_path, tokenizer, encoder, decoder)

    return result


def convert_images(model_path, folder_path):
    """
    Преобразует изображения в текстовый формат по определенному классу изображения
    :param model_path: Путь к сохраненной модели
    :param folder_path: Путь к папке с изображениями
    :return: Список преобразованных в текст формул
    """
    formulas = []
    latex_model = LatexOCR()
    converter = deepsmiles.Converter(rings=True, branches=True)
    model = tf.saved_model.load(model_path)
    infer = model.signatures["serving_default"]

    class_labels = [0, 1, 2] 

    for file_name in os.listdir(folder_path):
        if file_name.endswith('.jpg') or file_name.endswith('.jpeg') or file_name.endswith('.png'):
            img_path = os.path.join(folder_path, file_name)
            img = image.load_img(img_path, target_size=(100, 100))
            img_array = image.img_to_array(img)
            img_array = tf.expand_dims(img_array, 0)
            img_preprocessed = tf.keras.applications.resnet.preprocess_input(img_array)
            output = infer(conv2d_input=img_preprocessed)
            pred_class_idx = np.argmax(output['dense_2'][0])
            pred_class_label = class_labels[pred_class_idx]

            if pred_class_label == 1:
                img = Image.open(img_path)
                formulas.append(latex_model(img))
            else:
                try:
                    result = predictor(img_path)
                    formulas.append(''.join(result))
                except Exception as e:
                    print(e)
                    pass

    return formulas

