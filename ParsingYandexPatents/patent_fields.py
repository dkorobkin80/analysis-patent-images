from bs4 import BeautifulSoup
from webdriver import WebDriver
from typing import *
from os.path import basename


# patents_image = r'/patents_image'


class PatentFields:

    def __init__(self, web_driver: WebDriver) -> None:
        self.__soup: BeautifulSoup
        self.__web_driver = web_driver

    def set_patent_link(self, patent_link) -> None:
        page = self.__web_driver.get_patent_html(patent_link)
        self.__soup = BeautifulSoup(page, "html.parser")

    def get_id(self) -> str:
        tags = self.__soup.find_all('div', {'class': 'doc-summary-url'})
        return ''.join([tag.text for tag in tags]).replace(' ', '')

    def get_title(self) -> str:
        return self.__soup.find_all('div', {'class': 'document-title'}).pop().text.capitalize()

    def get_assignee(self) -> str:
        return self.__soup.find_all(lambda tag: 'Патентообладатели:' in tag.text).pop().next_sibling.text

    def get_authors(self) -> List[str]:
        return list(self.__soup.find_all(lambda tag: 'Авторы:' in tag.text).pop().next_sibling.stripped_strings)

    def get_publications(self) -> List[str]:
        req_date = self.__soup.find_all(lambda tag: 'Дата подачи заявки: ' in tag.text) \
            .pop().next_sibling.text.strip().replace('.', '-')
        resp_date = self.__soup.find_all(lambda tag: 'Опубликовано: ' in tag.text) \
            .pop().next_sibling.text.strip().replace('.', '-')
        return [req_date, resp_date]

    def get_abstract(self) -> str:
        tag = self.__soup.find(id='doc-abstract')
        abstract = ''
        if tag is not None:
            abstract = tag.next_sibling.text
        return abstract

    def get_description(self) -> str:
        tag = self.__soup.find(id='doc-description')
        description = ''
        if tag is not None:
            description = tag.next_sibling.text
        return description

    def get_claims(self) -> str:
        tag = self.__soup.find(id='doc-claims')
        claims = ''
        if tag is not None:
            claims = tag.next_sibling.text
        return claims

    def get_classification(self) -> List[str]:
        mpk_items = self.__soup.find_all('div', {'class': 'header-mpk-item'})[1:]
        return [mpk_item.contents[0].replace(' ', '') for mpk_item in mpk_items]

    def get_cited(self) -> List[str]:
        tag = self.__soup.find(id='doc-table-cited-by')
        cited = []
        if tag is not None:
            for cited_patent in tag.find_next_sibling('div').find_all(
                    'div',
                    {'class': 'doctable_row_click'}
            ):
                cited.append(cited_patent.find_next('span').text)
        return cited


